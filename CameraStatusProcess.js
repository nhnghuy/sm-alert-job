const CFG = require('./config/default.json')
const Redis = require('ioredis')
const mongoose = require('mongoose')
const Alert = require('./model/alert')
const rule = require('./config/CameraStatusRule.json')
const data = require('./data/CameraPingEvent.json')

// init connections
const _redis = new Redis(CFG.REDIS.PORT, CFG.REDIS.HOST)
mongoose.connect(CFG.MONGO.CONNECTION_STRING, {useNewUrlParser: true})
let db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
  .once('open', () => console.log('MongoDb connected !'))


const Status = {
    SUCCESS: 'Success',
    FAILURE: 'Failure'
}

const doAlert = async (cameraPingEvent, cameraStatusRule) => {
    // read status from cameraPingEvent
    console.info(`cameraPingEvent.status : ${cameraPingEvent.status}`)
    console.info(`cameraStatusRule.SuccessStatus : ${cameraStatusRule.SuccessStatus}`)
    let checkingStatus = cameraPingEvent.status == cameraStatusRule.SuccessStatus
                         ? Status.SUCCESS
                         : Status.FAILURE

    // read consecutive count from cache
    const cacheKey = `${cameraPingEvent.warehouseId}_${cameraPingEvent.cameraId}`
    console.info(`cache key : ${cacheKey}`)
    let cacheValue = await _redis.get(cacheKey)
    console.info(`cache value : ${cacheValue}`)

    // If the cache count exist, parse it to json. Otherwise, initialize it.
    let consecutive = cacheValue ? await JSON.parse(cacheValue) 
                                 : {status: Status.SUCCESS, count: 0}

    console.info(`checkingStatus : ${checkingStatus}`)
    console.info(`consecutive status : ${consecutive.status}`)
    console.info(`consecutive : ${JSON.stringify(consecutive)}`)
    // check current status
    if (checkingStatus === consecutive.status) {
        consecutive.count++
    } else {
        consecutive.status = checkingStatus
        consecutive.count = 1
    }
    console.info(`consecutive after update : ${await JSON.stringify(consecutive)}`)

    // check with threshold
    let threshold = consecutive.status == Status.SUCCESS ? cameraStatusRule.SuccessThreshold
                                                         : cameraStatusRule.FailureThreshold

    if (consecutive.count >= threshold) {
        console.info('Reach threshold')
        // reset count
        consecutive.count = 0

        // initialize alert & save it to db
        let alert = new Alert({
            warehouseId: cameraPingEvent.warehouseId,
            cameraId: cameraPingEvent.cameraId,
            event: consecutive.status == Status.SUCCESS ? 'CameraConnect' : 'CameraDisconnect',
            level: consecutive.status == Status.SUCCESS ? 'INFO' : 'WARN',
            message: consecutive.status == Status.SUCCESS ? 'Camera Connect' : 'Camera Disconnect',
            createdAt: new Date()
        })

        console.info('Try to save to DB...')
        try {
            let doc = await alert.save()
            console.info(`Alert has been written to DB, Id = [${doc._id}]`)
        } catch(err) {
            console.error(`Error when try to save : ${err}`)
        }
    }

    // update count to cache
    _redis.set(cacheKey, await JSON.stringify(consecutive))
}

async function main() {
    console.log('\n\n=====================================')
    await doAlert(data.success, rule)
    console.log('\n\n=====================================')
    await doAlert(data.success, rule)
    console.log('\n\n=====================================')
    await doAlert(data.failure, rule)
    console.log('\n\n=====================================')
    await doAlert(data.failure, rule)
    console.log('\n\n=====================================')
    await doAlert(data.success, rule)
    
    await process.exit()
}

main()

// module.exports = doAlert