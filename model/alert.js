const mongoose = require('mongoose')

const alertSchema = new mongoose.Schema({
    warehouseId: String,
    cameraId: String,
    event: String,
    level: String,
    message: String,
    pushedTime: String,
    createdAt: Date,
    updatedAt: Date
})

const alert = mongoose.model('Alert', alertSchema, 'alert', true)

module.exports = alert